//
//  School.swift
//  20190215-AparnaKishan-NYCSchools
//
//  Created by Jithin Prakash on 2/15/19.
//  Copyright © 2019 Aparna kishan. All rights reserved.
//

struct School {
    var school_name: String
    var city: String?
    var phone_number: String?
    var website: String?

}
